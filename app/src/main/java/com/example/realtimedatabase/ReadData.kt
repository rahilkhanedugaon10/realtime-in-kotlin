package com.example.realtimedatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.realtimedatabase.databinding.ActivityMainBinding
import com.example.realtimedatabase.databinding.ActivityReadDataBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class ReadData : AppCompatActivity() {
    private lateinit var binding:ActivityReadDataBinding
    private lateinit var database : DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadDataBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.readdataBtn.setOnClickListener {
            val userName: String = binding.etusername.text.toString()
            if (userName.isNotEmpty()) {
                readData(userName)
            } else {
                Toast.makeText(this, "Please enter the UserName", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun readData(userName: String) {
        database = FirebaseDatabase.getInstance().getReference("Users")
        database.child(userName).get().addOnSuccessListener {
            if (it.exists()){
                val firstname = it.child("firstName").value
                val lastName = it.child("lastName").value
                val age = it.child("age").value
                Toast.makeText(this, "Sucessfuly Read", Toast.LENGTH_SHORT).show()
                binding.etusername.text.clear()
                binding.tvFirstName.text =  firstname.toString()
                binding.tvLastName.text =  lastName.toString()
                binding.tvAge.text = age.toString()


            }else{
                Toast.makeText(this, "User Doesn't Exist", Toast.LENGTH_SHORT).show()

            }
        }.addOnFailureListener {
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()

        }
    }
}
