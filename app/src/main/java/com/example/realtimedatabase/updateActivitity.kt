package com.example.realtimedatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.realtimedatabase.databinding.ActivityUpdateActivitityBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class updateActivitity : AppCompatActivity() {
    private lateinit var binding: ActivityUpdateActivitityBinding
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateActivitityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.updateBtn.setOnClickListener {

            val userName = binding.userName.text.toString()
            val firstName = binding.firstName.text.toString()
            val lastName = binding.lastName.text.toString()
            val age = binding.age.text.toString()

            updateActivitity(userName, firstName, lastName, age)
        }
    }

    private fun updateActivitity(
        userName: String,
        firstName: String,
        lastName: String,
        age: String
    ) {
        database = FirebaseDatabase.getInstance().getReference("Users")
        val user = mapOf<String, String>(
            "firstName" to firstName,
            "lastName" to lastName,
            "age" to age
        )
        database.child(userName).updateChildren(user).addOnSuccessListener {
            binding.userName.text.clear()
            binding.firstName.text.clear()
            binding.lastName.text.clear()
            binding.age.text.clear()
            Toast.makeText(this, "Update tro successful", Toast.LENGTH_SHORT).show()


        }.addOnFailureListener {
            Toast.makeText(this, "Failed to update", Toast.LENGTH_SHORT).show()
        }
    }
}


