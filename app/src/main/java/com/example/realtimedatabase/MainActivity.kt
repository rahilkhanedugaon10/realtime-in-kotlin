package com.example.realtimedatabase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.realtimedatabase.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.registerBtn.setOnClickListener {
            if (binding.firstName.text.isNotEmpty()&& binding.lastName.text.isNotEmpty()&& binding.age.text.isNotEmpty()){
                register()
            }else{
                Toast.makeText(this, "fill database field", Toast.LENGTH_SHORT).show()
            }

        }
        binding.readBtn.setOnClickListener {
            startActivity(Intent(this, ReadData::class.java))
        }
        binding.updateBtn.setOnClickListener {
            startActivity(Intent(this,updateActivitity::class.java))
        }
        binding.deleteBtn.setOnClickListener {
            startActivity(Intent(this,DeleteData::class.java))
        }
        binding.recyclearBtn.setOnClickListener {
            startActivity(Intent(this,RecyclearVeiwActivity::class.java))
        }
    }

    private fun register() {
        val firstName = binding.firstName.text.toString()
        val lastName = binding.lastName.text.toString()
        val age = binding.age.text.toString()
        val userName = binding.userName.text.toString()

        database = FirebaseDatabase.getInstance().getReference("Users")
        val User = User(firstName, lastName, age, userName)
        database.child(userName).setValue(User).addOnSuccessListener {
            binding.firstName.text.clear()
            binding.lastName.text.clear()
            binding.age.text.clear()
            binding.userName.text.clear()

            Toast.makeText(this, "Successfully Saved", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
        }
    }
}